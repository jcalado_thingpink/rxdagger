package cr.legend.rxdagger.list;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.util.List;

import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.databinding.ListItemNoteBinding;

class NotesAdapter extends RecyclerView.Adapter<NotesAdapter.ViewHolder> {
    private List<Note> notes;
    private OnClickCallback mCallback;

    public NotesAdapter(List<Note> notes) {
        this.notes = notes;
    }

    public NotesAdapter() {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(ListItemNoteBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ListItemNoteBinding binding = holder.getBinding();
        Note note = getItem(position);

        binding.title.setText(note.getTitle());
        binding.text.setText(note.getText());
        if(note.getImageUrl() != null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(note.getImageUrl())).setResizeOptions(ResizeOptions.forDimensions(1200, 200)).build();
            DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                    .setOldController(binding.image.getController())
                    .setImageRequest(imageRequest)
                    .build();
            binding.image.setController(draweeController);
        }
    }

    @Override
    public int getItemCount() {
        return notes != null ? notes.size() : 0;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
        notifyDataSetChanged();
    }

    private Note getItem(int position) {
        return this.notes.get(position);
    }


    ///////////////////////////////////////////////////////////////////////////
    // CLICK LISTENER
    ///////////////////////////////////////////////////////////////////////////

    public OnClickCallback getCallback() {
        return mCallback;
    }

    public void setCallback(OnClickCallback mCallback) {
        this.mCallback = mCallback;
    }

    public interface OnClickCallback {
        void showDetails(final Note note);
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEWHOLDER
    ///////////////////////////////////////////////////////////////////////////

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ListItemNoteBinding mBinding;

        ViewHolder(ListItemNoteBinding viewDataBinding) {
            super(viewDataBinding.getRoot());

            mBinding = viewDataBinding;
            viewDataBinding.getRoot().setOnClickListener(this);
        }

        ListItemNoteBinding getBinding() {
            return mBinding;
        }

        @Override
        public void onClick(View view) {
            final Note store = NotesAdapter.this.notes.get(getAdapterPosition());
            switch (view.getId()) {

                default:
                    getCallback().showDetails(store);
                    break;
            }
        }
    }
}
