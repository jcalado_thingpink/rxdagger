package cr.legend.rxdagger.list;

import cr.legend.rxdagger.dao.INotesDao;
import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.dao.responses.NotesResponse;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;

class MainPresenter implements MainContract.Presenter {
    private MainContract.View mView;
    private INotesDao mNotesDao;
    private CompositeDisposable mCompositeDisposable;

    MainPresenter(MainContract.View view, INotesDao notesDao) {
        mView = view;
        mNotesDao = notesDao;
        mCompositeDisposable = new CompositeDisposable();
        mView.showLoading();
    }

    @Override
    public void getNotes() {
        Observable<NotesResponse> notesResponseObservable = mNotesDao.getAllNotes();

        Disposable disposable = notesResponseObservable
                .observeOn(AndroidSchedulers.mainThread())
                .firstOrError()
                .subscribeWith(new DisposableSingleObserver<NotesResponse>() {
                    @Override
                    public void onSuccess(NotesResponse notesResponse) {
                        mView.showNotes(notesResponse.getNotes());
                        mView.showContent();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e);
                    }
                });

        mCompositeDisposable.add(disposable);
    }

    @Override
    public void onDestroy() {
        rxUnSubscribe();
    }

    @Override
    public void showNoteDetails(Note note) {
        mView.showDetail(note);
    }

    private void rxUnSubscribe() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
    }
}
