package cr.legend.rxdagger.list;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.widget.Toast;

import java.util.List;

import cr.legend.rxdagger.R;
import cr.legend.rxdagger.dao.NotesDao;
import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.databinding.ActivityMainBinding;
import cr.legend.rxdagger.misc.SeparatorDecoration;
import cr.legend.rxdagger.note.DetailActivity;

public class MainActivity extends AppCompatActivity implements MainContract.View, NotesAdapter.OnClickCallback {
    private ActivityMainBinding mBinding;
    private NotesAdapter mAdapter;
    private MainContract.Presenter mPresenter;
    private LinearLayoutManager mLayoutManager;

    ///////////////////////////////////////////////////////////////////////////
    // ACTIVITY
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        setupPresenter();
        setupUI();
        setupData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // SETUP
    ///////////////////////////////////////////////////////////////////////////

    private void setupPresenter() {
        mPresenter = new MainPresenter(this, new NotesDao(this));
    }

    private void setupData() {
        mPresenter.getNotes();
    }

    private void setupUI() {
        mAdapter = new NotesAdapter();

        mBinding.notes.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        SeparatorDecoration dividerItemDecoration = new SeparatorDecoration(this, Color.TRANSPARENT, 10);

        mBinding.notes.setLayoutManager(mLayoutManager);
        mBinding.notes.addItemDecoration(dividerItemDecoration);
        mBinding.notes.setAdapter(mAdapter);
        mAdapter.setCallback(this);
    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEW
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showNotes(List<Note> notes) {
        mAdapter.setNotes(notes);
    }

    @Override
    public void showLoading() {
        mBinding.loadingProgress.show();
    }

    @Override
    public void showContent() {
        mBinding.loadingProgress.hide();
    }

    @Override
    public void showError(Throwable e) {
        Toast.makeText(this, "Whoopsy daisy", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDetail(Note note) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_NOTE_ID, note.getId());
        startActivity(intent);
    }
    ///////////////////////////////////////////////////////////////////////////
    // NOTES ADAPTER ON CLICK CALLBACK
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showDetails(Note note) {
        mPresenter.showNoteDetails(note);
    }
}
