package cr.legend.rxdagger.list;

import java.util.List;

import cr.legend.rxdagger.dao.model.Note;

interface MainContract {
    interface View {
        void showNotes(List<Note> notes);

        void showLoading();

        void showContent();

        void showError(Throwable e);

        void showDetail(Note id);
    }

    interface Presenter {
        void getNotes();

        void onDestroy();

        void showNoteDetails(Note note);
    }
}
