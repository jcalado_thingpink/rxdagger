package cr.legend.rxdagger.dao.model;

import com.google.gson.annotations.SerializedName;

public class Note {
    private int id;
    private String text;
    private String title;
    @SerializedName("image_url")
    private String imageUrl;

    public Note() {
    }

    public Note(String title, String text, String url) {
        this.title = title;
        this.text = text;
        this.imageUrl = url;
    }

    public int getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
