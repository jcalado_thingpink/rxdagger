package cr.legend.rxdagger.dao.mock;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import cr.legend.rxdagger.dao.INotesDao;
import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.dao.responses.NotesResponse;
import io.reactivex.Observable;
import io.reactivex.Single;

public class LocalMockNotesDao implements INotesDao {

    @Override
    public Observable<NotesResponse> getAllNotes() {
        //on subscription, call() is executed
        return Observable.fromCallable(new Callable<NotesResponse>() {
            @Override
            public NotesResponse call() throws Exception {
                ArrayList<Note> notes = new ArrayList<>();
                notes.add(new Note("Dummy 1", "This is my first dummy description", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Mallard2.jpg/1200px-Mallard2.jpg"));
                notes.add(new Note("Dummy 2", "This is my second dummy description", "http://kids.nationalgeographic.com/content/dam/kids/photos/animals/Birds/H-P/mallard-male-standing.adapt.945.1.jpg"));
                notes.add(new Note("Dummy 3", "This is my third dummy description", "https://deltadailynews.com/wp-content/uploads/2016/06/ddn-duck.jpg"));

                return new NotesResponse(notes);
            }
        });
    }

    @Override
    public Observable<Note> getNote(int id) {
        //emits a note and then completes
        return Observable.just(new Note("Dummy 1", "This is my first dummy description", "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a1/Mallard2.jpg/1200px-Mallard2.jpg"));
    }
}
