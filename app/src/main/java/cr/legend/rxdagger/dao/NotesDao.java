package cr.legend.rxdagger.dao;

import android.content.Context;
import android.support.v4.util.LruCache;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.backends.okhttp3.OkHttpImagePipelineConfigFactory;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.fatboyindustrial.gsonjodatime.DateTimeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import java.io.File;
import java.io.IOException;

import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.dao.responses.NotesResponse;
import cr.legend.rxdagger.dao.service.NotesService;
import cr.legend.rxdagger.utils.Utils;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;

public class NotesDao implements INotesDao {

    public static final String APIARY_MOCK_BASE_URL = "http://private-e8c8d-jorgecaladocr7.apiary-mock.com/";
    private NotesService mNotesService;
    private Retrofit mRetrofit;

    //Reuse Observables
    private LruCache<Class<?>, Observable<?>> apiObservables = new LruCache<>(10);

    private Interceptor getCacheControlInterceptor(final Context context) {
        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response originalResponse = chain.proceed(chain.request());
                if (Utils.isNetworkAvailable(context)) {
                    int maxAge = 60; // read from cache for 1 minute
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, max-age=" + maxAge)
                            .build();
                } else {
                    int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
                    return originalResponse.newBuilder()
                            .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                            .build();
                }
            }
        };
    }

    public NotesDao(Context context) {
        this(context, APIARY_MOCK_BASE_URL);
    }

    public NotesDao(Context context, String baseUrl) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        Gson gson = gsonBuilder.create();

        //setup logging
        Timber.plant(new Timber.DebugTree());
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.tag("OkHttp").d(message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);

        //setup cache
        File httpCacheDirectory = new File(context.getCacheDir(), "responses");
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache fileCache = new Cache(httpCacheDirectory, cacheSize);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .addInterceptor(logging)
                .addNetworkInterceptor(getCacheControlInterceptor(context))
                .cache(fileCache)
                .build();

        //setup Fresco
        ImagePipelineConfig config = OkHttpImagePipelineConfigFactory
                .newBuilder(context, okHttpClient)
                .build();
        Fresco.initialize(context, config);

        //setup retrofit
        mRetrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();

        //setup service implementation
        mNotesService = mRetrofit.create(NotesService.class);
    }

    public Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?>
            clazz, boolean cacheObservable, boolean useCache) {
        Observable<?> preparedObservable = null;

        if (useCache) //this way we don't reset anything in the cache if this is the only instance of us not wanting to use it.
            preparedObservable = apiObservables.get(clazz);

        if (preparedObservable != null)
            return preparedObservable;

        //we are here because we have never created this observable before or we didn't want to use the cache...

        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }

        return preparedObservable;
    }

    public void getNotes() {

    }

    public NotesService getAPI() {
        return mNotesService;
    }

    @Override
    public Observable<NotesResponse> getAllNotes() {
        return (Observable<NotesResponse>) getPreparedObservable(getAPI().getAllNotes(), NotesResponse.class, true, true);
    }

    @Override
    public Observable<Note> getNote(int id) {
        return (Observable<Note>) getPreparedObservable(getAPI().getNote(id), NotesResponse.class, true, true);
    }
}
