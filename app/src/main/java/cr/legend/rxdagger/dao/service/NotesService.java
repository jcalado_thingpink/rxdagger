package cr.legend.rxdagger.dao.service;

import java.util.List;

import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.dao.responses.NotesResponse;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface NotesService {

    @GET("notes")
    Observable<NotesResponse> getAllNotes();

    @GET("notes/{id}")
    Observable<Note> getNote(@Path("id") int id);

    @POST("notes")
    Observable<Note> submitNote();
}
