package cr.legend.rxdagger.dao;

import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.dao.responses.NotesResponse;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface INotesDao {
    Observable<NotesResponse> getAllNotes();

    Observable<Note> getNote(int id);
}
