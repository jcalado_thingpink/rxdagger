package cr.legend.rxdagger.dao.responses;

import java.util.ArrayList;
import java.util.List;

import cr.legend.rxdagger.dao.model.Note;

/**
 * Created by jorgecalado on 07/09/2017.
 */

public class NotesResponse {
    private List<Note> notes;

    public NotesResponse(ArrayList<Note> notes) {
        this.notes = notes;
    }

    public List<Note> getNotes() {
        return notes;
    }
}
