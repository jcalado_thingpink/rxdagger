package cr.legend.rxdagger.note;

import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.interfaces.DraweeController;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import cr.legend.rxdagger.R;
import cr.legend.rxdagger.dao.NotesDao;
import cr.legend.rxdagger.dao.model.Note;
import cr.legend.rxdagger.databinding.ActivityDetailBinding;

public class DetailActivity extends AppCompatActivity implements DetailContract.View {
    public static final String EXTRA_NOTE_ID = "extra.note_id";

    private ActivityDetailBinding mBinding;
    private DetailContract.Presenter mPresenter;

    ///////////////////////////////////////////////////////////////////////////
    // ACTIVITY
    ///////////////////////////////////////////////////////////////////////////

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_detail);

        setupPresenter();
        setupUI();
        setupData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    ///////////////////////////////////////////////////////////////////////////
    // SETUP
    ///////////////////////////////////////////////////////////////////////////

    private void setupPresenter() {
        mPresenter = new DetailPresenter(this, new NotesDao(this));
    }

    private void setupData() {
        int id = getIntent().getIntExtra(EXTRA_NOTE_ID, -1);

        mPresenter.getNote(id);
    }

    private void setupUI() {

    }

    ///////////////////////////////////////////////////////////////////////////
    // VIEW
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public void showNote(Note note) {
        mBinding.title.setText(note.getTitle());
        mBinding.text.setText(note.getText());
        if(note.getImageUrl() != null) {
            ImageRequest imageRequest = ImageRequestBuilder.newBuilderWithSource(Uri.parse(note.getImageUrl())).setResizeOptions(ResizeOptions.forDimensions(1200, 200)).build();
            DraweeController draweeController = Fresco.newDraweeControllerBuilder()
                    .setOldController(mBinding.image.getController())
                    .setImageRequest(imageRequest)
                    .build();
            mBinding.image.setController(draweeController);
        }
    }

    @Override
    public void showLoading() {
        mBinding.loadingProgress.show();
    }

    @Override
    public void showContent() {
        mBinding.loadingProgress.hide();
    }

    @Override
    public void showError(Throwable e) {
        Toast.makeText(this, "Whoopsy daisy", Toast.LENGTH_SHORT).show();
    }
}

