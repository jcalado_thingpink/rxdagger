package cr.legend.rxdagger.note;

import cr.legend.rxdagger.dao.model.Note;

/**
 * Created by jorgecalado on 12/09/2017.
 */

public interface DetailContract {
    public interface Presenter {
        void onDestroy();

        void getNote(int id);
    }

    public interface View {
        void showNote(Note note);

        void showLoading();

        void showContent();

        void showError(Throwable e);
    }
}
