package cr.legend.rxdagger.note;

import android.os.SystemClock;

import cr.legend.rxdagger.dao.INotesDao;
import cr.legend.rxdagger.dao.model.Note;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

class DetailPresenter implements DetailContract.Presenter {
    private final CompositeDisposable mCompositeDisposable;
    private INotesDao mNotesDao;
    private DetailContract.View mView;

    DetailPresenter(DetailContract.View view, INotesDao notesDao) {
        mView = view;
        mNotesDao = notesDao;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    public void onDestroy() {
        rxUnSubscribe();
    }

    private void rxUnSubscribe() {
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed())
            mCompositeDisposable.dispose();
    }

    @Override
    public void getNote(int id) {
        mView.showLoading();

        Observable<Note> noteResponseObservable = mNotesDao.getNote(id);
        DisposableSingleObserver<Note> disposableSingleObserver = noteResponseObservable
                .subscribeOn(AndroidSchedulers.mainThread())
                .firstOrError()
                .subscribeWith(new DisposableSingleObserver<Note>() {
                    @Override
                    public void onSuccess(Note note) {
                        mView.showNote(note);
                        showContentWithDelay();
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.showError(e);
                    }
                });

        mCompositeDisposable.add(disposableSingleObserver);
    }

    private void showContentWithDelay() {
        Disposable subscribeDisposable = Observable
                .fromCallable(() -> {
                    SystemClock.sleep(2000);
                    return false;
                })
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((result) -> mView.showContent());
        mCompositeDisposable.add(subscribeDisposable);
    }
}
